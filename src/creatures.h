/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CREATURES_H
#define CREATURES_H

#include "constants.h"
#include "player.h"

class Wolf
{
    private:                                    //There is probably no reason to use private members here, but whatever
        const static unsigned short obstinacy = 40;     //The higher it is, the more unlikely the wolves are to change direction.
#ifndef DEBUG
        unsigned short speed = 1;   //Defines how many fields the wolf moves at once.
#endif
        unsigned short target = 1; //Decides what is the wolf's target. 0 = random, 1 = player.
        short targetX;     //Decides where the wolf wants to go
        short targetY;
        
        unsigned short cooldownModifier = 16;       //Divided by difficulty
        unsigned short cooldown = 0;
        
    public:
        short positionX;   //Self-explanatory
        short positionY;
#ifdef DEBUG                //Needed for debug information
        unsigned short speed = 1;
#endif
        short damage = 1;   //Damage done by wolf
        
        void Move(char * terrain, Player &player, const short SIZE_X, const short SIZE_Y);  //Used every time the wolf should move
        
        short Maul(Player &player);     //Used at the end of every turn to decide if (and how much) damage should be dealt
        
        Wolf(char * terrain, const short SIZE_X, const short SIZE_Y, const unsigned short difficulty, Player &player);
};

#endif
//Called "creatures" for legacy reasons