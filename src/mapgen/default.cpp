//This thing generates the terrain in the "game"
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>

#include "../constants.h"
#include "default.h"

void generateDefault (char * terrain, const short SIZE_X, const short SIZE_Y)
{
    for (short y = 0; y < SIZE_Y; y++)
    {
        for (short x = 0; x < SIZE_X; x++)
        {
            if (rand() % 60 == 0)
            {
                short tempX = x;
                short tempY = y;
                for (unsigned short counter = 0; counter < 100; counter++)  //Generates a "lump" of rock and stone
                {
                    if (tempX >= 0 && tempX < SIZE_X && tempY >= 0 && tempY < SIZE_Y)
                    {
                        terrain[tempX+(SIZE_X*tempY)] = 'O';
                        tempX += (rand() % 3) - 1;
                        tempY += (rand() % 3) - 1;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else if (rand() % 40 == 0)
            {
                terrain[x+(SIZE_X*y)] = 'm';
            }
            else
            {
                terrain[x+(SIZE_X*y)] = ' ';
            }
        }
    }
}