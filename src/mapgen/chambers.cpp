//This thing generates the terrain in the "game"
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>

#include "../constants.h"
#include "chambers.h"

void generateChambers (char * terrain, const short SIZE_X, const short SIZE_Y)
{
    unsigned short chamberSize = 1;
    const unsigned attemptLimit = (SIZE_X * SIZE_Y) / 100;
    short chamberX;
    short chamberY;
    char fillWith = ' ';    //What the chamber should be filled with
    
    for (short y = 0; y < SIZE_Y; y++)          //Fill everything with X
    {
        for (short x = 0; x < SIZE_X; x++)
        {
            terrain[x+(SIZE_X*y)] = 'X';
        }
    }
    
    for (unsigned attempt = 0; attempt < attemptLimit; attempt++)
    {
        if (rand() % 25 == 0)   fillWith = 'm';
        else                    fillWith = ' ';
        
        chamberX = (rand() % (SIZE_X - 8)) + 4;               //Find random place for the chamber
        chamberY = (rand() % (SIZE_Y - 8)) + 4;
        
        switch (rand() % 10)
        {
            case 0:                                             //10% chance for "big" chamber
                chamberSize = 3;
                break;
            case 1:                                             //20% for "moderate-size" chamber
            case 2:
                chamberSize = 2;
                break;
            default:                                            //70% for "small" chamber
                chamberSize = 1;
        }
        
        for (short drillY = chamberY - chamberSize; drillY <= chamberY + chamberSize; drillY++)
        {
            for (short drillX = chamberX - chamberSize; drillX <= chamberX + chamberSize; drillX++)
            {
                terrain[drillX+(SIZE_X*drillY)] = fillWith;
            }
        }
    }
}