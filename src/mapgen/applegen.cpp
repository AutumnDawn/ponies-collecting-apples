/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include "applegen.h"

void appleGen(char *terrain, unsigned short appleTarget, const short SIZE_X, const short SIZE_Y)
{
    short x;
    short y;
    unsigned short applesGenerated = 0;
    unsigned short attempt = 0;


    while (applesGenerated < appleTarget && attempt < 1000) //Create apples, 1000 error tolerance
    {
        x = rand() % SIZE_X;
        y = rand() % SIZE_Y;
        
        if (terrain[x+(SIZE_X*y)] == ' ' || ( attempt > 900 && terrain[x+(SIZE_X*y)] != 'a'))   //If attempts exceed 900,
        {                                                                                       //try placing them everywhere
            terrain[x+(SIZE_X*y)] = 'a';
            applesGenerated++;
        }
        else
        {
            attempt++;
        }
    }
}