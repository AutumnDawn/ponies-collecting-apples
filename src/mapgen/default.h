/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DEFAULT_MAPGEN_H
#define DEFAULT_MAPGEN_H

#include "../constants.h"

void generateDefault(char * terrain, const short SIZE_X, const short SIZE_Y);

#endif