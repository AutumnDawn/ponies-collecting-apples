//Will probably need to rewrite this or something
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <cmath>
#include "../constants.h"
#include "tunnels.h"

void generateTunnels(char * terrain, const short SIZE_X, const short SIZE_Y)
{
    unsigned short direction = rand() % 4 + 1;  //Defines where the "digger" goes
    short diggerX = SIZE_X / 2;             //Start somewhere in the middle
    short diggerY = SIZE_Y / 2;

    char fieldFill = ' ';       //Defines what digger should fill the field with
    const unsigned int attemptMax = (SIZE_X * SIZE_Y) / 40;


    for (short y = 0; y < SIZE_Y; y++)      //Everything is rock by default
    {
        for (short x = 0; x < SIZE_X; x++)
        {
            terrain[x+(SIZE_X*y)] = 'O';
        }
    }
    
    for (unsigned int attempt = 0; attempt < attemptMax; attempt++)
    {
        if (rand() % 50 == 0)           //2% chance for field to be filled with traps
        {
            fieldFill = 'm';
        }
        else
        {
            fieldFill = ' ';
        }
        for (short y = diggerY - 1; y <= diggerY + 1; y++)      //Clears a 3x3 area around the "digger"
        {
            for (short x = diggerX - 1; x <= diggerX + 1; x++)
            {
                terrain[x+(SIZE_X*y)] = fieldFill;
            }
        }
        
        if ((diggerY < 6 || diggerY > SIZE_Y - 7) &&        //If in the corner,
            (diggerX < 6 || diggerX > SIZE_X - 7))          //go back to center, because I'm lazy
        {
            diggerX = SIZE_X / 2;
            diggerY = SIZE_Y / 2;
        }
        else if (diggerX < 6)               //Else, if near the edge, find another direction
        {
            switch(rand() % 3)
            {
                case 0:
                    direction = NORTH;
                    break;
                case 1:
                    direction = SOUTH;
                    break;
                case 2:
                    direction = EAST;
            }
        }
        else if (diggerX > SIZE_X - 7)
        {
            switch(rand() % 3)
            {
                case 0:
                    direction = NORTH;
                    break;
                case 1:
                    direction = SOUTH;
                    break;
                case 2:
                    direction = WEST;
            }
        }
        else if (diggerY < 6)
        {
            switch(rand() % 3)
            {
                case 0:
                    direction = WEST;
                    break;
                case 1:
                    direction = EAST;
                    break;
                case 2:
                    direction = SOUTH; 
            }
        }
        else if (diggerY > SIZE_Y - 7)
        {
            switch(rand() % 3)
            {
                case 0:
                    direction = WEST;
                    break;
                case 1:
                    direction = EAST;
                    break;
                case 2:
                    direction = NORTH; 
            }
        }
        
        switch (direction)  //Decides where to go based on direction
        {
            case NORTH:
                diggerY -= 3;
                break;
            case SOUTH:
                diggerY += 3;
                break;
            case WEST:
                diggerX -= 3;
                break;
            case EAST:
                diggerX += 3;
        }
        
        if (rand() % 20 == 0 || attempt % 20 == 0)  //Reroll direction, if walked too long in one direction,
        {                                       //or random chance
            direction = rand() % 4 + 1;
        }
    }
}