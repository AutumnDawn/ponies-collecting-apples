#ifndef CONSTANTS_H
#define CONSTANTS_H

#define APPLE_TARGET 5

#define ACTIVATED 10000

#define EP_COOLDOWN 3
#define UNI_COOLDOWN 3              //Multiplied by distance travelled and difficulty
#define PEGA_COOLDOWN 10            //Multiplied by difficulty, but not by travel distance
                                    //Forgive me the inconsistences, they're miserable

#define STAY 0
#define NORTH 1
#define SOUTH 2
#define WEST 3
#define EAST 4

#endif

//This is supposed to be basic definition macros, but I guess they should be moved to configuration file instead. TODO I guess.