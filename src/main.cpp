//TODO make everything less of a mess, move terrain-related things to its own class.
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <ctime>
#include <string>
#include <vector>

#include "constants.h"
#include "config.h"
#include "player.h"
#include "creatures.h"
#include "render.h"
#include "mapgen/default.h"
#include "mapgen/tunnels.h"
#include "mapgen/chambers.h"
#include "mapgen/applegen.h"

int main()
{
    short SIZE_X = 40, SIZE_Y = 20;
    char mapType = 'd';                         //Defines what type of map to generate
    
    Player player;                                          //This initialization seems messy, need to fix that
    Render render;
    
    unsigned short difficultyMultiplier = 2;
    int deltaTime;                          //For calculating time, at the end
    unsigned short appleTarget = APPLE_TARGET;
    int seed;
    char input = 0;
    
    std::string afterturnMessage = "";
    std::string difficultyName = "medium";
    
    configure(SIZE_X, SIZE_Y, mapType);     //Configure basic things at start
    
    do                                                          //Choose your species here
    {
        std::cout
        << "Please choose species ('u' for unicorn, 'p' for pegasus, 'e' for earth pony): ";
        std::cin >> player.species;
        if (player.species != 'u' && player.species != 'p' && player.species != 'e')
        {
            std::cout << "Incorrect value.\n";
        }
    }
    while (player.species != 'u' && player.species != 'p' && player.species != 'e');
    
    {                                                       //Choose difficulty and map type here
        char difficulty = 'm';
        std::cout << "Choose difficulty (e/M/h): ";         //Medium is already initiated
        std::cin >> difficulty;
        
        if (difficulty == 'e' || difficulty == 'E')
        {
            difficultyMultiplier = 1;
            difficultyName = "easy";
        }
        else if (difficulty == 'h' || difficulty == 'H')
        {
            difficultyMultiplier = 4;
            difficultyName = "hard";
        }
        else if (difficulty == 'u' || difficulty == 'U')    //"Secret" difficulty
        {
            difficultyMultiplier = 8;
            difficultyName = "unapproachable";
            std::cout << "Good luck, have fun.\n";
        }
    }
    
    std::cout << "Input seed (integer) ('1' for random): ";
    std::cin >> seed;
    if (seed == 1)
        seed = time(0);
    srand(seed);
    
    {                                                           //Initialization of things just before main loop
        char terrain[SIZE_Y][SIZE_X];                       //Terrain array
        
        std::vector<Wolf> wolf;
        
        unsigned turn = 0;
        unsigned short apples = 0;
        bool frameRender = false;
        const int timeStart = time(0);
        
        switch(mapType)             //Terrain generation
        {                           //Needs to go before wolf and player position defining
            case 'T':
            case 't':
                generateTunnels(*terrain, SIZE_X, SIZE_Y);
                break;
            case 'D':
            case 'd':
                generateDefault(*terrain, SIZE_X, SIZE_Y);
                break;
            case 'C':
            case 'c':
                generateChambers(*terrain, SIZE_X, SIZE_Y);
                break;
            default:
                std::cout << "Invalid map type, using default.\n";
                generateDefault(*terrain, SIZE_X, SIZE_Y);
        }
        appleTarget *= difficultyMultiplier;                //Define how many apples should spawn
        appleGen(*terrain, appleTarget, SIZE_X, SIZE_Y);    //Generates apples
                                                            //Bonus (negative) points for naming consistency
        
        player.Spawn(*terrain, difficultyMultiplier, SIZE_X, SIZE_Y);               //Configure the player
        
        for (unsigned short counter = 0; counter < difficultyMultiplier; counter++) //Configure the wolves
        {
            wolf.push_back(Wolf(*terrain, SIZE_X, SIZE_Y, difficultyMultiplier, player));
        }
        
        if (SIZE_X > 80 || SIZE_Y > 40)
            frameRender = true;
        
        if (frameRender)
            render.frame(*terrain, player, wolf, SIZE_X, SIZE_Y);
        else
            render.full(*terrain, player, wolf, SIZE_X, SIZE_Y);     //Render the main screen
        
        while (input != 'e' && apples < appleTarget && player.health > 0)   //Main loop
        {
            std::cin >> input;
            
            switch (input)                       //Checks for input and acts upon it
            {
                case 'w':
                    player.Move(*terrain, NORTH, afterturnMessage, apples, SIZE_X, SIZE_Y);
                    break;
                case 's':
                    player.Move(*terrain, SOUTH, afterturnMessage, apples, SIZE_X, SIZE_Y);
                    break;
                case 'a':
                    player.Move(*terrain, WEST, afterturnMessage, apples, SIZE_X, SIZE_Y);
                    break;
                case 'd':
                    player.Move(*terrain, EAST, afterturnMessage, apples, SIZE_X, SIZE_Y);
                    break;
                
                case 'f':           //Activate the ability
                player.Ability(afterturnMessage);
                break;
                
                case 'g':           //Modify the special ability.
                player.Adjust(afterturnMessage);
                break;
#ifdef DEBUG
                case '.':               //Debug, don't use pls
                    std::cout << "(DEBUG) player.cooldownModifier: "
                    << player.cooldownModifier << "\n";
                    std::cout << "(DEBUG) player.distanceModifier: "
                    << player.distanceModifier << "\n";
                    std::cout << "(DEBUG) player.ability: "
                    << player.ability << "\n";
                    break;
                case ',':
                    if (frameRender)
                        frameRender = false;
                    else
                        frameRender = true;
                    break;
                case '<':
                    player.health += 100;
                    break;
                case '?':
                    for (unsigned short counter = 0; counter < wolf.size(); counter++)
                    {
                        std::cout << "(DEBUG) wolf.speed: "
                        << wolf[counter].speed << "\n";
                    }
                    break;
                case 't':
                    std::cin >> player.positionX >> player.positionY;
                    break;
#endif
                case 'h':
                    afterturnMessage.append("Available keys:\nw, a, s, d => movement\n");
                    afterturnMessage.append("g => adjust ability\nf => use ability\n");
                    break;
                case 'e':         //Quit the game
                    break;
                
                default:
                {
                    afterturnMessage.append("Incorrect input.\n");
                }
            }
            
            if ( input == 'w' || input == 's' || input == 'a' || input == 'd' )     //Processes what should happen if a player attempts to move.
            {                                                                       //Notice that activating/cancelling ability does not trigger it.
                const short initialHealth = player.health;
                unsigned short addTurn = 1;             //How many turn iterations should be performed
                
                if (player.species == 'p' && player.abilityUsed)        //Pegasi's ability performs several turn iterations
                {
                    addTurn = (player.distanceModifier / 2);    //Amount of turn iterations is around half of travelled distance
                }
                
                for (unsigned short turnIteration = 0; turnIteration < addTurn; turnIteration++)    //Performs as many iterations as needed
                {
                    for (unsigned short wolfCounter = 0; wolfCounter < wolf.size(); wolfCounter++)  //Moves wolves
                    {
                        wolf[wolfCounter].Move(*terrain, player, SIZE_X, SIZE_Y);
                        player.health -= wolf[wolfCounter].Maul(player);
                    }
                }
                
                if (player.ability != 0 && player.ability < ACTIVATED)   //Checks if ability should cool down by one turn.
                {
                    player.ability--;
                }
                
                if (initialHealth > player.health)
                {
                    afterturnMessage.append("You have been damaged.\n");
                }
                
                player.abilityUsed = false;     //So it doesn't apply with next turn
                turn += addTurn;
            }
            
            if (frameRender)
            {
                render.frame(*terrain, player, wolf, SIZE_X, SIZE_Y);
            }
            else
            {
                render.full(*terrain, player, wolf, SIZE_X, SIZE_Y);        //Render the screen
            }
            
            if (afterturnMessage.empty())       //So the display keeps constant position
                afterturnMessage = "\n";
            std::cout << afterturnMessage;
            
            if (player.ability != 0 && player.ability < ACTIVATED)
            {
                std::cout << "Cooldown: " << player.ability << ".\n";
            }
            std::cout << "Health: " << player.health << ".\t"
            << "Position: " << player.positionX << " " << player.positionY << ".\n"
            << "Apples: " << apples << "/" << appleTarget << ".\t"
            << "Turn: " << turn << ".\n"
            << player.speciesName << " at " << difficultyName << ".\n";
            
            afterturnMessage.clear();
        }                               //Main loop ends
        
        deltaTime = time(0) - timeStart;                        //Display time of the game
        std::cout << "Time: " << deltaTime / 60 << " minutes, "
        << deltaTime % 60 << " seconds.\n";
        
        std::cout << "Turns: " << turn << ".\n";
        
        if (player.health <= 0)                                 //In case you lost
        {
            std::cout << "You have fainted.\n";
            if (player.health < -20)
            {
                std::cout << "\nThough in your case,\n"
                "I'm not sure if \"fainted\" is appropriate.\n";
            }
            if (player.health < -70)
            {
                std::cout << "I'm not sure anything child-friendly is appropriate anymore.\n";
            }
            if (turn <= 1 && difficultyMultiplier <= 4)
            {
                std::cout << "\nYou have managed to faint in just one turn, somehow.\n"
                "There is a possibility that a wolf spawned right next to you,\n"
                "and if that's the case, tell Autumn Dawn that she sucks.\n\n"
                "If you did it intentionally, however, I congratulate your curiosity,\n"
                "though I wouldn't advise being *that* curious in reality.\n";
            }
        }
        if (apples >= appleTarget)                              //In case you won
        {
            std::cout << "You have collected enough apples.\n";
        }
    }  
    return 0;
}