//Player class.
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cstdlib>

#include "constants.h"
#include "player.h"

#define MAX_ABILITY_DISTANCE 10     //The maximum value you can set for special ability

#define BLOCKED "You cannot go any further.\n"
#define COLLECTED "Apple collected.\n"
#define DESTROYED "Obstacle destroyed.\n"
        
void Player::Move(char * terrain, unsigned short direction, std::string &afterturnMessage, unsigned short &apples, const short SIZE_X, const short SIZE_Y)
{
    unsigned short modifier = 1;
    char * targetField;
    
    if (ability == ACTIVATED)       //Defines what movement modifiers to apply
    {
        switch (species)
        {
            case 'u':
                modifier = distanceModifier;
                ability = cooldownModifier * distanceModifier;  //Unis' cooldown affected by distance and difficulty
                break;
            case 'p':
                modifier = distanceModifier;
                ability = cooldownModifier;                     //Pegasi's cooldown is affected only by difficulty
                abilityUsed = true;
                break;
            case 'e':
                modifier = distanceModifier;
                ability = cooldownModifier;                     //...same for earthponies (their ability isn't adjustable anyways)
                break;
            default:
                afterturnMessage.append("ERROR: Unknown player species, aborting.\n"); //Shouldn't happen
                return;
        }
    }
    
    switch (direction)      //Defines what target field is, or denies movement if said field would be outside map
    {
        case NORTH:
            if (positionY < modifier)
            {
                afterturnMessage.append(BLOCKED);
                return;
            }
            else
            {
                targetField = &(terrain[ SIZE_X*(positionY - modifier) + positionX ]);
            }
            break;
        case SOUTH:
            if (positionY >= SIZE_Y - modifier)
            {
                afterturnMessage.append(BLOCKED);
                return;
            }
            else
            {
                targetField = &(terrain[ SIZE_X*(positionY + modifier) + positionX ]);
            }
            break;
        case WEST:
            if (positionX < modifier)
            {
                afterturnMessage.append(BLOCKED);
                return;
            }
            else
            {
                targetField = &(terrain[ SIZE_X*positionY + (positionX - modifier) ]);
            }
            break;
        case EAST:
            if (positionX >= SIZE_X - modifier)
            {
                afterturnMessage.append(BLOCKED);
                return;
            }
            else
            {
                targetField = &(terrain[ SIZE_X*positionY + (positionX + modifier) ]);
            }
            break;
        default:
            afterturnMessage.append("ERROR: unidentified direction.\n");
            return;
    }
    

    switch (*targetField)       //Defines what to do with you based on the target field
    {
        case 'X':                   //Soft material ("soft stone" or "sand" or "clay" or whatever you prefer to call it)
            if (ability == 0)
            {
                if (species != 'e') ability = 3;    //So the digging is "slower"
                *targetField = ' ';
                afterturnMessage.append(DESTROYED);
            }
            else    afterturnMessage.append(BLOCKED);
            break;
            
        case 'm':
            if (species == 'e') {
                *targetField = ' ';
                afterturnMessage.append(DESTROYED);
            } else {
                health = 0;
            }
            break;
            
        case 'O':           //Standard rock and stone
            if (species == 'e' && ability == 0)
            {
                ability = cooldownModifier;
                *targetField = ' ';
                afterturnMessage.append(DESTROYED);
            } else {
                afterturnMessage.append(BLOCKED);
            }
            break;
            
        case 'a':   //Apple collection
            apples++;
            *targetField = ' ';
            afterturnMessage.append(COLLECTED);
        case ' ':   //Move
            switch (direction)
            {
                case NORTH:
                    positionY -= modifier;
                    break;
                case SOUTH:
                    positionY += modifier;
                    break;
                case WEST:
                    positionX -= modifier;
                    break;
                case EAST:
                    positionX += modifier;
                    break;
            }
            break;
        default:
            afterturnMessage.append("ERROR: unidentified material.\n");
    }
}
        
void Player::Ability(std::string &afterturnMessage)                                                                                 //Processes ability activation/deactivation
{
    switch(ability)                                                                                                                 //Checks the state of ability. todo move to player class
        {
            case 0:                                                                                                                 //Activate the ability in case it's eglible
                ability = ACTIVATED;
                afterturnMessage.append("Your ability is now activated and will be applied with your next move.\n");
                break;
            case ACTIVATED:                                                                                                         //Return ability back to eglible state when f is pressed after activation, cancelling it
                ability = 0;
                afterturnMessage.append("Cancelled.\n");
                break;
            default:                                                                                                                //Otherwise tell that the ability is not ready
                afterturnMessage.append("Ability not ready.\n");
        }
}
        
void Player::Adjust(std::string &afterturnMessage)
{
    if (species == 'e')
    {
            afterturnMessage.append("Your ability isn't adjustable.\n");
    }
    else
    {
        do
        {
            std::cout << "Please select range: ";
            std::cin >> distanceModifier;
            if (distanceModifier > MAX_ABILITY_DISTANCE)
            {
                std::cout << "Selected distance is our of range.\n";
            }
        } while (distanceModifier > MAX_ABILITY_DISTANCE || distanceModifier < 2);  //Max distance is MAX_ABILITY_RANGE, which is defined in config.h
    }
}

void Player::Spawn(char * terrain, const unsigned short difficultyMultiplier, const short SIZE_X, const short SIZE_Y)  //This should probably be constructor,
{                                                                           //but for some reason isn't
    short x;
    short y;
    
    for (unsigned counter = 0; counter < 500; counter++) //If spawning player fails after X attempts, give up to prevent neverending loop
    {
        x = rand() % SIZE_X;
        y = rand() % SIZE_Y;
        if (terrain[x+(SIZE_X*y)] == ' ')
        {
            positionX = x;
            positionY = y;
            break;
        }
    }
    switch (species)                        //Apply species-specific variables.
    {
        case 'e':
            distanceModifier = 2;           //Cannot be changed during the game
            cooldownModifier = EP_COOLDOWN;
            health = 5;
            speciesName = "Earthpony";
            break;
        case 'p':
            distanceModifier = 3;
            cooldownModifier = PEGA_COOLDOWN;
            health = 3;
            speciesName = "Pegasus";
            break;
        case 'u':
            distanceModifier = 3;
            cooldownModifier = UNI_COOLDOWN;
            health = 3;
            speciesName = "Unicorn";
            break;
    }
    cooldownModifier *= difficultyMultiplier;
}

#undef MAX_ABILITY_DISTANCE
#undef BLOCKED
#undef COLLECTED
#undef DESTROYED