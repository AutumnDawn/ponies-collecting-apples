//Supposed to be responsible for loading config,
//such as map size.
//I have no idea what's happening here anyways,
//this thing is an abomination.
//TODO MAKE THIS BETTER.
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "config.h"

void configure(short &SIZE_X, short &SIZE_Y, char &mapType)
{
    const short DEFAULT_X = 40;
    const short DEFAULT_Y = 20;
    const char DEFAULT_MAP = 'd';
    
    const short TUNNELS_X = 200;
    const short TUNNELS_Y = 100;
    const char TUNNELS_MAP = 't';
    
    const short CHAMBERS_X = 100;
    const short CHAMBERS_Y = 50;
    const char CHAMBERS_MAP = 'c';
    
    short tempX;
    short tempY;
    char tempMap;

    std::fstream configFile("pca-config", std::fstream::in);
    
    if (configFile >> tempX && configFile >> tempY &&
        configFile >> tempMap)   //If both X and Y, as well as map type are found in the config file, apply them
    {
        if ( tempX >= 7 && tempX <= 30000 &&  //Field can be neither too small nor too big
            tempY >= 7 && tempY <= 30000 )
        {
            SIZE_X = tempX;
            SIZE_Y = tempY;
            mapType = tempMap;
            return;                 //Return so the config file creation doesn't activate,
        }                           //and yes, I know it's terrible
        else
        {
            std::cout << "ERROR: values in config file out of range.\n";
        }
    }                               //When it fails at both previous ones,
                                    //and yes, I know it's all trash code
    configFile.close();
    
    configFile.open("pca-config", std::fstream::out);
    char choice;
    
    std::cout << "Config file not found or invalid.\n"
    << "Choose config template:\n"
    << "> Default, 40x20\n"
    << "> Tunnels, 200x100\n"
    << "> Chambers, 100x50\n"
    << "(D/t/c) ";
    std::cin >> choice;
    
    if (choice == 't' || choice == 'T') //Give the player a choice
    {
        tempX = TUNNELS_X;
        tempY = TUNNELS_Y;
        tempMap = TUNNELS_MAP;
    }
    else if (choice == 'c' || choice == 'C')
    {
        tempX = CHAMBERS_X;
        tempY = CHAMBERS_Y;
        tempMap = CHAMBERS_MAP;
    }
    else
    {
        tempX = DEFAULT_X;
        tempY = DEFAULT_Y;
        tempMap = DEFAULT_MAP;
    }
    
    configFile << tempX << " " << tempY << " " << tempMap;
    configFile.close();
    SIZE_X = tempX;
    SIZE_Y = tempY;
    mapType = tempMap;
}