//This is supposed to render the screen to the screen
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "constants.h"
#include "render.h"

void Render::full (char * terrain, Player &player, std::vector<Wolf> &wolf, const short SIZE_X, const short SIZE_Y)
{
    char tempTerrain[SIZE_Y][SIZE_X];
    
    for (short y = 0; y < SIZE_Y; y++)                                                     //Process the natural terrain
    {
        for (short x = 0; x < SIZE_X; x++)
        {
            tempTerrain[y][x] = terrain[x+(SIZE_X*y)];
        }
    }
    
    tempTerrain[player.positionY][player.positionX] = 'P';                                  //"Process" the player
    
    for (unsigned short counter = 0; counter < wolf.size(); counter++)                      //Process the wolves
    {
        tempTerrain[wolf[counter].positionY][wolf[counter].positionX] = 'W';
    }
    
    for (short y = 0; y < SIZE_Y; y++)                                                     //Display the results to the screen
    {
        for (short x = 0; x < SIZE_X; x++)
        {
            display += tempTerrain[y][x];
        }
        display.append("\n");
    }
    std::cout << display;
    display.clear();
}

#define TEMP_SIZE_X 81      //Odd, so the player is in the middle
#define TEMP_SIZE_Y 41

#define BORDER_X 40
#define BORDER_Y 20

void Render::frame ( char * terrain,                          //Renders only a frame of the screen,
                   Player &player, std::vector<Wolf> &wolf, //if the field size is very big.
                   const short SIZE_X, const short SIZE_Y)  //TODO either delete, or rewrite.
{
    char tempTerrain[TEMP_SIZE_Y][TEMP_SIZE_X];

    for (short y = -BORDER_Y; y <= BORDER_Y; y++)
    {
        for (short x = -BORDER_X; x <= BORDER_X; x++)   //This may seem terrible, but I'm only warming up
        {
            if (player.positionX + x < 0 || player.positionX + x >= SIZE_X ||
                player.positionY + y < 0 || player.positionY + y >= SIZE_Y)
            {
                tempTerrain[y + BORDER_Y][x + BORDER_X] = '#';
            }
            else
            {
                tempTerrain[y + BORDER_Y][x + BORDER_X] = terrain[ (player.positionX + x) +
                ( (player.positionY + y) * SIZE_X ) ];        //KILL ME, END MY SUFFERING
            }
        }
    }                   //I think we're past the worst now
    
    tempTerrain[BORDER_Y][BORDER_X] = 'P';    //Player always in center
    
    for (unsigned short counter = 0; counter < wolf.size(); counter++)
    {
        if ( !( abs( wolf[counter].positionX - player.positionX ) > BORDER_X ||  //Check if the wolf is in the field I guess??? (kill me)
            abs( wolf[counter].positionY - player.positionY ) > BORDER_Y) )
        {
            tempTerrain[wolf[counter].positionY - player.positionY + BORDER_Y]
            [wolf[counter].positionX - player.positionX + BORDER_X] = 'W';
        }
    }           //Okay, now the worst is really past us, now just to display
                //the mess that was achieved through blood and suffering
    for (short y = 0; y < TEMP_SIZE_Y; y++)     //This time simpler, as we don't need anything more
    {
        for (short x = 0; x < TEMP_SIZE_X; x++)
        {
            display += tempTerrain[y][x];
        }
        display.append("\n");
    }
    std::cout << display;
    display.clear();
}               //The suffering finishes here, hopefully

#undef TEMP_SIZE_X
#undef TEMP_SIZE_Y

#undef BORDER_X
#undef BORDER_Y