//Class of "wolf", which is supposed to be an enemy in the game
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <cmath>

#include "constants.h"
#include "creatures.h"

#define RANDOM 0
#define PLAYER 1

void Wolf::Move (char * terrain, Player &player, const short SIZE_X, const short SIZE_Y)   //This spaghetti block of code is supposed to be the wolf's (terrble) AI. Definitely a rework todo.
{
    unsigned short direction = 0;   //Defines which direction the wolf should go. 0 = stay, 1 = north, 2 = south, 3 = west, 4 = east.
    char * targetField;
    
    if (target == RANDOM && rand() % (obstinacy / 2) == 0)  //I'm not sure how it works,
    {                                                       //but it's supposed to be responsible for
        target = PLAYER;                                    //changing the target
    }
    else if (target == PLAYER && rand() % obstinacy == 0)  
    {
        target = RANDOM;
        targetX = rand() % SIZE_X;
        targetY = rand() % SIZE_Y;
        speed = 1;
    }
    
    if (target == PLAYER)
    {
        targetX = player.positionX;
        targetY = player.positionY;
        
        if (abs( positionX - targetX ) > 40 || abs( positionY - targetY ) > 20) //Wolf is faster when far away
        {                                                                       //when the target is player
            speed = 2;
        }
        else
        {
            speed = 1;
        }
    }
    else if (targetX == positionX && targetY == positionY)  //Wolf will chase player after arriving at target
    {
        target = PLAYER;
    }
    
    for (short moveIteration = 0; moveIteration < speed; moveIteration++)           //Moves as many times as speed,
    {                                                                               //which is either 1 or 2, but I can't think of anything better
        if (targetX == positionX)               //Target on same X
        {
            if (targetY > positionY)            //Target to the south
            {
                direction = SOUTH;
            }
            else                                //Target to the north
            {
                direction = NORTH;
            }
        }
        else if (targetX > positionX)           //Target is more to the east
        {
            if (targetY > positionY)            //Target to the south-east
            {
                if (rand() % 2 == 0)
                {
                    direction = SOUTH;
                }
                else
                {
                    direction = EAST;
                }
            }
            else if (targetY < positionY)       //Target to the north-east
            {
                if (rand() % 2 == 0)
                {
                    direction = NORTH;
                }
                else
                {
                    direction = EAST;
                }
            }
            else                                //Target is directly to the east
            {
                direction = EAST;
            }
        }
        else                                    //Target is more to the west
        {
            if (targetY > positionY)            //Target to the south-west
            {
                if (rand() % 2 == 0)
                {
                    direction = SOUTH;
                }
                else
                {
                    direction = WEST;
                }
            }
            else if (targetY < positionY)       //Target to the north-west
            {
                if (rand() % 2 == 0)
                {
                    direction = NORTH;
                }
                else
                {
                    direction = WEST;
                }
            }
            else                            //Target is directly to the west
            {
                direction = WEST;
            }
        }
        
        switch (direction)                  //This is probably the uglies piece of code in the entire thing.
        {                                   //It applies movement decided in the previous part; or not, if there's an obstacle.
            case NORTH:
                targetField = &terrain[positionX+(SIZE_X*(positionY-1))];
                break;
            
            case SOUTH:
                targetField = &terrain[positionX+(SIZE_X*(positionY+1))];
                break;
            
            case WEST:
                targetField = &terrain[(positionX-1)+(SIZE_X*positionY)];
                break;
            
            case EAST:
                targetField = &terrain[(positionX+1)+(SIZE_X*positionY)];
                break;
        }
        
        switch(*targetField) {
            case 'X':
                if (cooldown > 0) {
                    cooldown--;
                } else {
                    *targetField = ' ';
                    cooldown = cooldownModifier;
                }
                break;
            case 'a':
            case ' ':
                switch (direction) {        //Yes, this is copied (and slightly adjusted) from player.cpp; and yes, I should do better
                    case NORTH:
                        positionY--;
                        break;
                    case SOUTH:
                        positionY++;
                        break;
                    case WEST:
                        positionX--;
                        break;
                    case EAST:
                        positionX++;
                        break;
                }
                break;
            case 'm':
            case 'O':
                break;          //TODO do something when they don't find a way; idk, maybe better pathfinding or something
        }
    }
}

short Wolf::Maul(Player &player)    //Decide how much damage to deal to player, if at all
{
    if ( abs(positionX - player.positionX) + abs(positionY - player.positionY) <= 1 )   //Check if player is in adjacent field.
    {                                                                                   //Thanks to Maze for better formula.
        return damage;
    }
    else
    {
        return 0;
    }
}

Wolf::Wolf (char * terrain, const short SIZE_X, const short SIZE_Y, const unsigned short difficulty, Player &player)    //Kinda messy (like everything here)
{
    damage = difficulty;
    cooldownModifier /= difficulty;
    
    for (unsigned attempt = 0; attempt < 500; attempt++) {
        positionX = rand() % SIZE_X;
        positionY = rand() % SIZE_Y;
        
        if (terrain[positionX + (SIZE_X*positionY)] == ' ' &&
            ( abs(positionX - player.positionX) > 2 && abs(positionY - player.positionY) > 2 )) { //So wolves don't spawn too near the player
            return;
        }
    }
    positionX = SIZE_X / 2;     //If all else fails, put them in the middle,
    positionY = SIZE_Y / 2;     //which for some reason doesn't seem to work
}

#undef RANDOM
#undef PLAYER