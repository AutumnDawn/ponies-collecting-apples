/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#include "constants.h"

#define MESSAGE std::string &afterturnMessage

class Player
{
    public:
        char species;               //Defines species/race
        std::string speciesName;    //Full name of the species
        
        short positionX;            //Defines player's position
        short positionY;
        
        unsigned short ability = 0;         //Defines the state of player's ability, 
        unsigned short distanceModifier;    //Defined by player input
        unsigned short cooldownModifier;    //Defined by race and difficulty
        
        bool abilityUsed = false;           //For checking if ability was used last turn,
                                            //currently used only for pegasi
        
        short health;
        
        void Move(char * terrain, unsigned short direction,
                  MESSAGE, unsigned short &apples,
                  const short SIZE_X, const short SIZE_Y); //For moving around. Direction is a number, where 1 = north, 2 = south,
                                                                                                            //3 = west, 4 = east. Wrong direction will give error.
                                                                                                            //TODO maybe turn into a string.
                                                                                                            
        void Ability(MESSAGE);                                                                              //For using abilities
        
        void Adjust(MESSAGE);                                                                               //For adjusting abilities
        
        void Spawn(char * terrain, unsigned short difficultyMultiplier, const short SIZE_X, const short SIZE_Y);
};

#undef MESSAGE
#endif