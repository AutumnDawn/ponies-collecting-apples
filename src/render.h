/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef RENDER_H
#define RENDER_H

#include <iostream>
#include <vector>
#include <cmath>
#include <string>

#include "constants.h"
#include "player.h"
#include "creatures.h"

class Render
{
private:
    std::string display;
public:
    void full (char * terrain, Player &player, std::vector<Wolf> &wolf, const short SIZE_X, const short SIZE_Y);

    void frame ( char * terrain,                          //Renders only a frame of the screen,
                        Player &player, std::vector<Wolf> &wolf, //if field size is very big
                        const short SIZE_X, const short SIZE_Y);
};

#endif