CC = g++
src = src/main.cpp src/player.cpp src/creatures.cpp src/render.cpp src/config.cpp src/mapgen/default.cpp src/mapgen/tunnels.cpp src/mapgen/applegen.cpp src/mapgen/chambers.cpp

pca: $(src)
	$(CC) $(src) -Wall -O2 -o ./pca

pca-debug: $(src)
	$(CC) $(src) -Wall -DDEBUG -o ./pca-debug